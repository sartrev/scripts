#!/bin/bash

git clone https://github.com/banga/powerline-shell.git

mkdir -p ~/.powerline-shell
cd powerline-shell 
sed -e "s/^#\(  *'set_term_title'\)/\1/" \
  -e "s/^\(  *'virtual_env'\)/#\1/" \
  -e "s/^\(  *'username'\)/#\1/" \
  -e "s/^\(  *'hostname'\)/#\1/" \
  -e "s/^\(  *'ssh'\)/#\1/" \
  -e "s/^#\(  *'cwd'\)/\1/" \
  -e "s/^#\(  *'read_only'\)/\1/" \
  -e "s/^#\(  *'git'\)/\1/" \
  -e "s/^#\(  *'root'\)/\1/" \
  -e "s/\(THEME *= *'\).*'/\1solarized-dark'/" config.py.dist > config.py

./install.py && cp powerline-shell.py ~/.powerline-shell/ && cd ..
rm -rf powerline-shell

os=`uname`
if test "$os" = "Darwin"; then
  bash_profile=~/.bash_profile
else
  bash_profile=~/.bashrc
fi

echo '' >> $bash_profile
echo 'function _update_ps1() {' >> $bash_profile
echo '    PS1="$(~/.powerline-shell/powerline-shell.py $? 2> /dev/null)"' >> $bash_profile
echo '}' >> $bash_profile
echo 'if [ "$TERM" != "linux" ]; then' >> $bash_profile
echo '    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"' >> $bash_profile
echo 'fi' >> $bash_profile
