#!/bin/bash

usage() {
    echo "USAGE:"
    echo "    $0 <user> <password>"
    echo ""
    exit 1
}

if test $# != 2; then
    usage
fi

user=$1
pass=$2

systemctl stop firewalld.service
systemctl disable firewalld.service

yum install -y ppp iptables pptpd
echo "localip 192.168.0.1" >> /etc/pptpd.conf
echo "remoteip 192.168.0.234-238,192.168.0.245" >> /etc/pptpd.conf
echo "ms-dns 8.8.8.8" >> /etc/ppp/options.pptpd
echo "ms-dns 8.8.4.4" >> /etc/ppp/options.pptpd
echo "$user    pptpd    $pass    *" >> /etc/ppp/chap-secrets
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
sysctl -p
iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o eth0 -j MASQUERADE
echo "iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o eth0 -j MASQUERADE" >> /etc/rc.d/rc.local

service pptpd start
chkconfig pptpd on
