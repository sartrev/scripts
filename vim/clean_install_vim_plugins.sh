#!/bin/bash

base_dir=$(cd "$(dirname $0)"; pwd)
uname=$(uname)
echo "Platform is $uname"

rm -f ~/.vimrc
rm -rf ~/.vim

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

if test $? != 0; then
    exit 1
fi

cp ${base_dir}/*.vim ~/.vim/
echo "source ~/.vim/vundle_config.vim" >> ~/.vimrc
vim +PluginInstall +qall
echo "source ~/.vim/plugins_config.vim" >> ~/.vimrc

# Install YouCompleteMe
#git clone --recursive https://github.com/Valloric/YouCompleteMe.git ~/.vim/bundle/YouCompleteMe/

#if test "$uname" = "Linux"; then
    #if type yum > /dev/null 2>&1; then
        #sudo yum -y install make automake gcc gcc-c++ kernel-devel cmake clang
        #if test $? -ne 0; then
            #echo "Install dependencies failed. Abort." && exit 1
        #fi
    #else
        #echo "Cannot install dependencies. Abort." && exit 1
    #fi

    #cd ~/.vim/bundle/YouCompleteMe && ./install.sh --clang-completer
#elif test "$uname" = "Darwin"; then
    #cd ~/.vim/bundle/YouCompleteMe && ./install.sh --clang-completer --system-libclang
#fi

