set nobackup
set fencs=utf-8,gbk

colorscheme dracula
syntax on
color dracula
filetype on
filetype plugin on
filetype plugin indent on
set autoindent
set smartindent
set showmatch
set ruler
set incsearch
set hlsearch
set nu
set ignorecase smartcase

set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2

" -- ctags --
" map <ctrl>+F12 to generate ctags for current folder:
map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR><CR>
" add current directory's generated tags file to available tags
set tags+=./tags

""""""""""""""""""
" TagList settings
""""""""""""""""""
let Tlist_Ctags_Cmd='ctags'
let Tlist_Show_One_File=1
let Tlist_Exit_OnlWindow=1
let Tlist_Use_Right_Window=1
let Tlist_Sort_type="name"
let Tlist_Close_On_Select=1
let Tlist_GainFocus_On_ToggleOpen=1


""""""""""""""""""
" MiniBufExploer settings
""""""""""""""""""
let g:miniBufExplMapWindowNavVim=1
let g:miniBufExplMapWindowNavArrows=1
let g:miniBufExplMapCTabSwitchBufs=1
let g:miniBufExplModSelTarget=1

"""""""""""""""""""""
" JavaBrower
"""""""""""""""""""""
let JavaBrower_Ctags_Cmd = "ctags"

"""""""""""""""""""""
" cscope
"""""""""""""""""""""

if has("cscope")
    if filereadable("cscope.out")
        cs add cscope.out
    endif
endif

"""""""""""""""""""""
" intent guides
"""""""""""""""""""""

let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_start_level=2
let g:indent_guides_guide_size=1

"""""""""""""""""""""
" Rainbow Parentheses
"""""""""""""""""""""

let g:rainbow_active = 1

"""""""""""""""""""""""""
" echofunc
"""""""""""""""""""""""""
let g:EchoFuncKeyNext='<C-n>'
let g:EchoFuncKeyPrev='<C-p>'

if(v:version >= 700)
    set laststatus=2
    if has('statusline')
        let g:EchoFuncShowOnStatus=1
        set statusline=%<%f\ %{EchoFuncGetStatusLine()}\ %h%m%r%=%-10.(%l,%c%V%)\ %P(%L)\
    endif
endif

"""""""""""""""""""""""""
" supertab
"""""""""""""""""""""""""
